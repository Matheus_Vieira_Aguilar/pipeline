<?php

use App\Manager\IndexManager;
use PHPUnit\Framework\TestCase;

/**
 * ClassNameTest
 * @group group
 */
class ManagerTest extends TestCase
{
    /** @test */
    public function test_function()
    {
        //manage
        $manager = new IndexManager();
        $menuTemplate = array("Avaliados", "erferufb", "Privilégios", "Ajuda");
        $menu = $manager->getMenu();
        $this->assertEquals($menuTemplate, $menu); 
    }
}
    