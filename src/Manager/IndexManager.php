<?php

namespace App\Manager;

class IndexManager
{
    /**
     * @param  void
     * @return array
     */
    public function getMenu()
    {
        $menus = array("Avaliados", "Gestor", "Privilégios", "Ajuda");
        return $menus;
    }
}
