<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Manager\IndexManager;

/**
 * Controller de Index
 */
class IndexController extends AbstractController
{

    /**
     * @Route("/index", name="index")
     */
    public function index()
    {
        // realizando mudanças:
        $manager = new IndexManager();
        $menus = $manager->getMenu();
        return $this->render(
            'index/index.html.twig',
            [
            'controller_name' => 'IndexController',
            'menus' => $menus
            ]
        );
    }
}
